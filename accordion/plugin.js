CKEDITOR.plugins.add( 'accordion', {

    requires: 'widget',

    init: function( editor ) {
   		
   		editor.widgets.add( 'accordion', {
            // Widget code.
            allowedContent: 'div[id];div(*);h2[id];h2(*)',
            editables: {
              title: {
                selector: '.content-accordion-title',
              },
              content: {
              	selector: '.content-accordion-content',
              }
            },
            template:
              '<div class="content-accordion">' +
                '<h2 class="content-accordion-title">Title</h2>' +
                '<div class="content-accordion-content"><p>Content</p></div>' +
              '</div>',
            upcast: function ( element ) {
              return element.name == 'div' && element.hasClass('content-accordion');
            },
        } );
   		
   		editor.ui.addButton( 'accordion', {
    		label: 'Accordion/Collapsible Content', //tooltip text for the button
    		command: 'accordion',
    		icon: this.path + 'icons/accordion.png'
   		});
   		
   		editor.config.contentsCss.push(CKEDITOR.getUrl(this.path + 'css/contents.css'));
    }
    
} );